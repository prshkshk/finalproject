/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Education.Organisation.SchoolAdministrativeOfficeOrganisation;
import Business.Education.Organisation.SchoolDeanOrganisation;
import Business.Organisation.Organisation;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public class SchoolEnterprise extends Enterprise {
    
    
    public SchoolEnterprise(String name){
    super(name,Enterprise.EnterpriseType.School);
    }
    
    @Override
    public ArrayList<Organisation> getSupportedOrganisation(){
        ArrayList<Organisation> Organisations = new ArrayList();
        Organisations.add(new SchoolDeanOrganisation());
        Organisations.add(new SchoolAdministrativeOfficeOrganisation());
        return Organisations;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
    return null;
    }
    
}

