/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organisation.Organisation;
import Business.Organisation.OrganisationDirectory;
import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public abstract class Enterprise extends Organisation {

    private EnterpriseType enterpriseType;
    private OrganisationDirectory organisationDirectory;

    public OrganisationDirectory getOrganisationDirectory() {
        return organisationDirectory;
    }

    public void setOrganizationDirectory(OrganisationDirectory organisationDirectory) {
        this.organisationDirectory = organisationDirectory;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }

    
    
    public abstract ArrayList<Organisation> getSupportedOrganisation();
    
    public enum EnterpriseType {

        Hospital("Hospital"),
        Government("Government"),
        School("School"),
        University("University");

        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        private EnterpriseType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }


    }

    public Enterprise(String name, EnterpriseType type) {
        super(name,null);
        this.enterpriseType = type;
        organisationDirectory= new OrganisationDirectory();
    }

}