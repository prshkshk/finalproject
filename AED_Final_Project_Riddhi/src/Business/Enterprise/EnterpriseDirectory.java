/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> enterpriseList; 

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> EnterpriseList) {
        this.enterpriseList = EnterpriseList;
    }
  
   public EnterpriseDirectory(){
   enterpriseList=new ArrayList<Enterprise>();
   
   } 
    
    //createEnterpise
    
public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
Enterprise enterprise=null;
if(type==Enterprise.EnterpriseType.Hospital)
{
       enterprise=new HospitalEnterprise(name);
       enterpriseList.add(enterprise);
}
else if(type==Enterprise.EnterpriseType.Government)
{
    enterprise=new GovernmentEnterprise(name);
    enterpriseList.add(enterprise);

}
else if(type==Enterprise.EnterpriseType.School)
{
    enterprise=new SchoolEnterprise(name);
    enterpriseList.add(enterprise);

}
else if (type==Enterprise.EnterpriseType.University)
{
    enterprise=new UniversityEnterprise(name);
    enterpriseList.add(enterprise);

}
return enterprise;

}

    
}
