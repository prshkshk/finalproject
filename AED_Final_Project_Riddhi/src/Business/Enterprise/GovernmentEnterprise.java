/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Citizen.Organisation.CitizenOrganisation;
import Business.FinanceDepartment.Organisation.FinanceDepartmentClerkOrganisation;
import Business.FinanceDepartment.Organisation.FinanceDepartmentOfficerOrganisation;
import Business.FinanceDepartment.Organisation.StateFinanceDepartmentMinisterOrganisation;
import Business.HRD.Organisation.HRDClerkOrganisation;
import Business.HRD.Organisation.HRDOfficerOrganisation;
import Business.HRD.Organisation.StateHRDMinisterOrganisation;
import Business.HealthDept.Organisation.HealthDepartmentClerkOrganisation;
import Business.HealthDept.Organisation.HealthDepartmentOfficerOrganisation;
import Business.HealthDept.Organisation.StateHealthMinisterOrganisation;
import Business.Organisation.Organisation;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public class GovernmentEnterprise extends Enterprise {
    
    public GovernmentEnterprise(String name){
    super(name,Enterprise.EnterpriseType.Government);
    }
    
    
    
    @Override
    public ArrayList<Role> getSupportedRole() {
    return null;
    }

    @Override
    public ArrayList<Organisation> getSupportedOrganisation() {
        ArrayList<Organisation> Organisations = new ArrayList();
        Organisations.add(new StateFinanceDepartmentMinisterOrganisation());
        Organisations.add(new FinanceDepartmentClerkOrganisation());
        Organisations.add(new FinanceDepartmentOfficerOrganisation());
        Organisations.add(new StateHRDMinisterOrganisation());
        Organisations.add(new HRDClerkOrganisation());
        Organisations.add(new HRDOfficerOrganisation());
        Organisations.add(new StateHealthMinisterOrganisation());
        Organisations.add(new HealthDepartmentClerkOrganisation());
        Organisations.add(new HealthDepartmentOfficerOrganisation());
        Organisations.add(new CitizenOrganisation());
        return Organisations;
    }
    
}
