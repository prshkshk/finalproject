/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Education.Organisation.UniversityAdministrativeOfficeOrganisation;
import Business.Education.Organisation.UniversityDeanOrganisation;
import Business.Organisation.Organisation;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public class UniversityEnterprise extends Enterprise {
    
    
    public UniversityEnterprise(String name){
    super(name,Enterprise.EnterpriseType.University);
    }
    
    @Override
    public ArrayList<Organisation> getSupportedOrganisation(){
        ArrayList<Organisation> Organisations = new ArrayList();
        Organisations.add(new UniversityAdministrativeOfficeOrganisation());
        Organisations.add(new UniversityDeanOrganisation());
        return Organisations;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
    return null;
    }
    
}