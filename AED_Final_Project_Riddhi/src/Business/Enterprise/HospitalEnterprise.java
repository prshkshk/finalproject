/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Hospital.Organisation.DoctorOrganisation;
import Business.Hospital.Organisation.HospitalAdministrativeOfficeOrganisation;
import Business.Hospital.Organisation.LabAssistantOrganisation;
import Business.Organisation.Organisation;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public class HospitalEnterprise extends Enterprise {
    
    
    public HospitalEnterprise(String name){
    super(name,Enterprise.EnterpriseType.Hospital);
    }
    
    @Override
    public ArrayList<Organisation> getSupportedOrganisation(){
        ArrayList<Organisation> Organisations = new ArrayList();
        Organisations.add(new DoctorOrganisation());
        Organisations.add(new HospitalAdministrativeOfficeOrganisation());
        Organisations.add(new LabAssistantOrganisation());
        return Organisations;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
    return null;
    }
    
}
