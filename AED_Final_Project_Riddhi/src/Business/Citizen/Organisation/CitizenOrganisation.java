/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Citizen.Organisation;

import Business.Organisation.Organisation;
import Business.Role.CitizenRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class CitizenOrganisation extends Organisation {
    public CitizenOrganisation() {
        super(Organisation.Type.Citizen.getValue(),Organisation.Type.Citizen);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new CitizenRole());
        return roles;
    }
}
