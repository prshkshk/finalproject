/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HRD.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.StateHRDMinisterRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class StateHRDMinisterOrganisation extends Organisation {

   public StateHRDMinisterOrganisation() {
        super(Organisation.Type.StateHRDMinister.getValue(),Organisation.Type.StateHRDMinister);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new StateHRDMinisterRole());
        return roles;
    }
    
}
