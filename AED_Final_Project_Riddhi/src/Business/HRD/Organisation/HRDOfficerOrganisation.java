/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HRD.Organisation;

import Business.Organisation.Organisation;
import static Business.Organisation.Organisation.Type.HRDOfficer;
import Business.Role.HRDOfficerRole;
import Business.Role.Role;
import static Business.Role.Role.RoleType.HRDOfficer;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class HRDOfficerOrganisation extends Organisation{

    public HRDOfficerOrganisation() {
        super(Organisation.Type.HRDOfficer.getValue(),Organisation.Type.HRDOfficer);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HRDOfficerRole());
        return roles;
    }
    
}
