/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HRD.Organisation;

import Business.Organisation.Organisation;
import Business.Role.HRDClerkRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class HRDClerkOrganisation extends Organisation {

    public HRDClerkOrganisation () {
         super(Organisation.Type.HRDClerk.getValue(),Organisation.Type.HRDClerk);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new HRDClerkRole());
        return roles;
    }
    
}
