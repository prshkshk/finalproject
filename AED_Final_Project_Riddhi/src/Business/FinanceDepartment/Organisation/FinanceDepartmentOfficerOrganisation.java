/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.FinanceDepartment.Organisation;

import Business.Organisation.Organisation;
import Business.Role.FinanceDepartmentOfficerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class FinanceDepartmentOfficerOrganisation extends Organisation {
    
    public FinanceDepartmentOfficerOrganisation() {
        super(Organisation.Type.FinanceDeptOfficer.getValue(),Organisation.Type.FinanceDeptOfficer);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new FinanceDepartmentOfficerRole());
        return roles;
    }
}
