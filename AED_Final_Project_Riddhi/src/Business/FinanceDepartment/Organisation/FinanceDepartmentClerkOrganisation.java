/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.FinanceDepartment.Organisation;

import Business.Organisation.Organisation;
import Business.Role.FinanceDepartmentClerkRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class FinanceDepartmentClerkOrganisation extends Organisation {
    
    public FinanceDepartmentClerkOrganisation() {
        super(Organisation.Type.FinanceDeptClerk.getValue(),Organisation.Type.FinanceDeptClerk);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new FinanceDepartmentClerkRole());
        return roles;
    }
}
