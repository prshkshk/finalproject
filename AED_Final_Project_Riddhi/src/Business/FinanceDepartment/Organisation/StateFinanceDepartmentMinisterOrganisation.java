/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.FinanceDepartment.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.StateFinanceMinisterRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class StateFinanceDepartmentMinisterOrganisation extends Organisation {
    
    public StateFinanceDepartmentMinisterOrganisation() {
        super(Organisation.Type.StateFinanceMinister.getValue(),Organisation.Type.StateFinanceMinister);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       //roles.add(new StateFinanceMinisterRole());
        return roles;
    }
    
}
