/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.NLP;

import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class NLP {

    private ArrayList<String> hrdKeywords;
    private ArrayList<String> financeDeptKeywords;
    private ArrayList<String> healthDeptKeywords;

    public NLP() {
        hrdKeywords = new ArrayList<>();
        hrdKeywords.add("School");
        hrdKeywords.add("University");
        hrdKeywords.add("College");
        hrdKeywords.add("Student");
        hrdKeywords.add("Teacher");
        hrdKeywords.add("Dean");
        hrdKeywords.add("Class");
        financeDeptKeywords = new ArrayList<>();
        financeDeptKeywords.add("Bank");
        financeDeptKeywords.add("Insurance");
        financeDeptKeywords.add("Money");
        financeDeptKeywords.add("Finance");
        financeDeptKeywords.add("Fraud");
        healthDeptKeywords = new ArrayList<>();
        healthDeptKeywords.add("Hospital");
        healthDeptKeywords.add("Doctor");
        healthDeptKeywords.add("Lab");
        healthDeptKeywords.add("Health");
        healthDeptKeywords.add("Operation");

    }

    public void addHRDKeyword(String keyword) {
        hrdKeywords.add(keyword);
    }

    public void addfinanceDeptKeyword(String keyword) {
        hrdKeywords.add(keyword);
    }

    public void addhealthDeptKeyword(String keyword) {
        hrdKeywords.add(keyword);
    }

    public ArrayList<String> getHrdKeywords() {
        return hrdKeywords;
    }

    public ArrayList<String> getFinanceDeptKeywords() {
        return financeDeptKeywords;
    }

    public ArrayList<String> getHealthDeptKeywords() {
        return healthDeptKeywords;
    }

}
