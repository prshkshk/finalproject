/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Riddhi
 */
public class FinanceDepartmentOfficerWorkRequest  extends WorkRequest{
    
    private FinanceDepartmentClerkWorkRequest financeClerkWorkRequest;
    private String result;
    

    public FinanceDepartmentClerkWorkRequest getfinanceClerkWorkRequest() {
        return financeClerkWorkRequest;
    }

    public void sethRDClerkWorkRequest(FinanceDepartmentClerkWorkRequest financeClerkWorkRequest) {
        this.financeClerkWorkRequest = financeClerkWorkRequest;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
   
    
    
    
    
    
}
