/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Riddhi
 */
public class HealthMinisterWorkRequest extends WorkRequest{
    
    
    
    private HealthOfficerWorkRequest healthOfficerWorkRequest;
    private String result;
    
    public HealthOfficerWorkRequest getHealthOfficerWorkRequest() {
        return healthOfficerWorkRequest;
    }

    public void setHealthOfficerWorkRequest(HealthOfficerWorkRequest healthOfficerWorkRequest) {
        this.healthOfficerWorkRequest = healthOfficerWorkRequest;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    
    
    
    
}
