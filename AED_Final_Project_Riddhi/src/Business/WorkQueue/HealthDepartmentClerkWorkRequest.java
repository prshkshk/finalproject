/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Riddhi
 */
public class HealthDepartmentClerkWorkRequest extends WorkRequest{
    
    private String citizenFeedback;
    private String result;
    GrievanceWorkRequest grievanceWorkRequest;

    public String getCitizenFeedback() {
        return citizenFeedback;
    }

    public void setCitizenFeedback(String citizenFeedback) {
        this.citizenFeedback = citizenFeedback;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public GrievanceWorkRequest getGrievanceWorkRequest() {
        return grievanceWorkRequest;
    }

    public void setGrievanceWorkRequest(GrievanceWorkRequest grievanceWorkRequest) {
        this.grievanceWorkRequest = grievanceWorkRequest;
    }

    
    
    
    
}
