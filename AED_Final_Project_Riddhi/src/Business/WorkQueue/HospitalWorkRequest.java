/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Acer
 */
public class HospitalWorkRequest extends WorkRequest{
    
    private String result;
    private String testResult;
    private HealthDepartmentClerkWorkRequest healthDepartmentClerkWorkRequest;
    private HealthOfficerWorkRequest healthOfficerWorkRequest;

    public HealthDepartmentClerkWorkRequest getHealthDepartmentClerkWorkRequest() {
        return healthDepartmentClerkWorkRequest;
    }

    public void setHealthDepartmentClerkWorkRequest(HealthDepartmentClerkWorkRequest healthDepartmentClerkWorkRequest) {
        this.healthDepartmentClerkWorkRequest = healthDepartmentClerkWorkRequest;
    }
    
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
    
    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public HealthOfficerWorkRequest getHealthOfficerWorkRequest() {
        return healthOfficerWorkRequest;
    }

    public void setHealthOfficerWorkRequest(HealthOfficerWorkRequest healthOfficerWorkRequest) {
        this.healthOfficerWorkRequest = healthOfficerWorkRequest;
    }
    
    
}
