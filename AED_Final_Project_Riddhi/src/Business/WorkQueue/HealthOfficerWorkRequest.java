/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Riddhi
 */
public class HealthOfficerWorkRequest extends WorkRequest{
    
    
    
    private String result;
    private HealthDepartmentClerkWorkRequest healthDepartmentClerkWorkRequest;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public HealthDepartmentClerkWorkRequest getHealthDepartmentClerkWorkRequest() {
        return healthDepartmentClerkWorkRequest;
    }

    public void setHealthDepartmentClerkWorkRequest(HealthDepartmentClerkWorkRequest healthDepartmentClerkWorkRequest) {
        this.healthDepartmentClerkWorkRequest = healthDepartmentClerkWorkRequest;
    }
    
    
    
    
}
