/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Riddhi
 */
public class SchoolAdminWorkRequest extends WorkRequest{
    
    private String testResult;
    HRDOfficerWorkRequest hRDOfficerWorkRequest;
    HRDClerkWorkRequest hRDClerkWorkRequest;

    public HRDClerkWorkRequest gethRDClerkWorkRequest() {
        return hRDClerkWorkRequest;
    }

    public void sethRDClerkWorkRequest(HRDClerkWorkRequest hRDClerkWorkRequest) {
        this.hRDClerkWorkRequest = hRDClerkWorkRequest;
    }
    

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public HRDOfficerWorkRequest gethRDOfficerWorkRequest() {
        return hRDOfficerWorkRequest;
    }

    public void sethRDOfficerWorkRequest(HRDOfficerWorkRequest hRDOfficerWorkRequest) {
        this.hRDOfficerWorkRequest = hRDOfficerWorkRequest;
    }

    
    
    
}

