/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Acer
 */
public class GrievanceWorkRequest extends WorkRequest{
    
    private String grievanceCategory;
    private String relatedOrganisation;
    private String feedback;
    private int feedbackScore;
    private String actiontaken;

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
    
    
    

    public String getGrievanceCategory() {
        return grievanceCategory;
    }

    public void setGrievanceCategory(String grievanceCategory) {
        this.grievanceCategory = grievanceCategory;
    }

    public String getRelatedOrganisation() {
        return relatedOrganisation;
    }

    public void setRelatedOrganisation(String relatedOrganisation) {
        this.relatedOrganisation = relatedOrganisation;
    }

    public int getFeedbackScore() {
        return feedbackScore;
    }

    public void setFeedbackScore(int feedbackScore) {
        this.feedbackScore = feedbackScore;
    }

    public String getActiontaken() {
        return actiontaken;
    }

    public void setActiontaken(String actiontaken) {
        this.actiontaken = actiontaken;
    }

   
    
    
}
