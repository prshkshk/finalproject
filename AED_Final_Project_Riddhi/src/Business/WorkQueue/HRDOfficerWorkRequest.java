/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Riddhi
 */
public class HRDOfficerWorkRequest  extends WorkRequest{
    
    private HRDClerkWorkRequest hRDClerkWorkRequest;
    private String result;
    

    public HRDClerkWorkRequest gethRDClerkWorkRequest() {
        return hRDClerkWorkRequest;
    }

    public void sethRDClerkWorkRequest(HRDClerkWorkRequest hRDClerkWorkRequest) {
        this.hRDClerkWorkRequest = hRDClerkWorkRequest;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
   
    
    
    
    
    
}
