/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author raunak
 */
public class LabTestWorkRequest extends WorkRequest{
    
    private String testResult;
    private HospitalWorkRequest hospitalWorkRequest;
    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public HospitalWorkRequest getHospitalWorkRequest() {
        return hospitalWorkRequest;
    }

    public void setHospitalWorkRequest(HospitalWorkRequest hospitalWorkRequest) {
        this.hospitalWorkRequest = hospitalWorkRequest;
    }
    
    
}
