/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author prshk
 */
public class DoctorWorkRequest extends WorkRequest{
    private String Result;
    private HospitalWorkRequest hospitalWorkRequest;

    public String getResult() {
        return Result;
    }

    public void setResult(String Result) {
        this.Result = Result;
    }

    public HospitalWorkRequest getHospitalWorkRequest() {
        return hospitalWorkRequest;
    }

    public void setHospitalWorkRequest(HospitalWorkRequest hospitalWorkRequest) {
        this.hospitalWorkRequest = hospitalWorkRequest;
    }
    
}
