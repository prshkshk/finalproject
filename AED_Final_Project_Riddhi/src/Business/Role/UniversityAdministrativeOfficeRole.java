/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Education.Organisation.UniversityAdministrativeOfficeOrganisation;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.Education.University.UniversityAdministrationWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Riddhi
 */
public class UniversityAdministrativeOfficeRole extends  Role{
  
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organisation, Enterprise enterprise, EcoSystem business,Network network) {
      return new UniversityAdministrationWorkAreaJPanel(userProcessContainer, account, (UniversityAdministrativeOfficeOrganisation)organisation, enterprise);
    }
    
    
}