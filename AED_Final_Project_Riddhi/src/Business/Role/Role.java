/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author Riddhi
 */
public abstract class Role {

    public enum RoleType {

        Admin("Admin"),
       
        HumanResouceMinister("HumanResouceMinister"),
        HealthMinister("HealthMinister"),
        FinanceMinister("FinanceMinister"),
        
        //Human Resource dept
        StateHRDMinister("StateHRDMinister"),
        HRDOfficer("HRDOfficer"),
        HRDClerk("HRDClerk"),
       
        //Health dept
        StateHealthMinister("StateHealthMinister"),
        HealthDepartmentOfficer("HealthDepartmentOfficer"),
        HealtchDepartmentClerk("HealthDepartmentClerk"),
        
        //FinanceMinister
        StateFinanceMinister("StateFinanceMinister"),
        FinanceDepartmentOfficer("FinanceDepartmentOfficer"),
        FinanceDepartmentClerk("FinanceDepartmentClerk"),
        
        //Education Enterprise  
        EducationMinister("EducationMinister"),
            
        //University
        UniversityDean("UniversityDean"),
        UniversityAdministrativeOffice("UniversityAdministrativeOffice"),
        
        //school
        SchoolDean("SchoolDean"),
        SchoolAdministrativeOffice("SchoolAdministrativeOffice"),
       
        //Hospital Enterpise
        LabAssistant("LabAssistant"),
        Doctor("Doctor"),
        Receptionsit("Receptionsit"),
        HospitalAdministrativeOffice("Hospital Administrative Office"),
        
        Citizen("Citizen");

        private String value;

        private RoleType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }

    }

    public abstract JPanel createWorkArea(JPanel userProcessContainer,
            UserAccount account,
            Organisation organisation,
            Enterprise enterprise,
            EcoSystem business,Network network);

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
