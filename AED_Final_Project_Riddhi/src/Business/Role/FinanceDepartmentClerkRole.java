/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.FinanceDepartment.Organisation.FinanceDepartmentClerkOrganisation;
import Business.Hospital.Organisation.DoctorOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.FinanceDepartment.FinanceDepartmentClerkWorkAreaJPanel;
import UserInterface.Hospital.DoctorRole.DoctorWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Riddhi
 */
public class FinanceDepartmentClerkRole extends Role{
  
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organization, Enterprise enterprise, EcoSystem business, Network network) {
      return new FinanceDepartmentClerkWorkAreaJPanel(userProcessContainer, account,
              (FinanceDepartmentClerkOrganisation)organization, enterprise, network);
    }
    
    
}