/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HRD.Organisation.HRDClerkOrganisation;
import Business.Hospital.Organisation.DoctorOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.HRD.HRDClerkWorkAreaJPanel;
import UserInterface.Hospital.DoctorRole.DoctorWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Riddhi
 */
public class HRDClerkRole extends Role{

  

   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organization, Enterprise enterprise, EcoSystem business,Network network) {
      return new HRDClerkWorkAreaJPanel(userProcessContainer, account, (HRDClerkOrganisation)organization, enterprise,network);
    }
    
    
}