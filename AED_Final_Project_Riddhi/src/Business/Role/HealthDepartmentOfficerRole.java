/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HRD.Organisation.HRDClerkOrganisation;
import Business.HealthDept.Organisation.HealthDepartmentOfficerOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.HRD.HRDClerkWorkAreaJPanel;
import UserInterface.HealthDept.HealthDepartmentOfficerWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Riddhi
 */
public class HealthDepartmentOfficerRole extends Role{
  
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organization, Enterprise enterprise, EcoSystem business, Network network) {
      return new HealthDepartmentOfficerWorkAreaJPanel(userProcessContainer, account, (HealthDepartmentOfficerOrganisation)organization, enterprise,network);
    }
    
    
}