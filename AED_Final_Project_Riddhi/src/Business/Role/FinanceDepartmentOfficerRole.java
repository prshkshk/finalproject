/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.FinanceDepartment.Organisation.FinanceDepartmentOfficerOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.FinanceDepartment.FinanceDepartmentOfficerWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Riddhi
 */
public class FinanceDepartmentOfficerRole extends Role{
  
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organization, Enterprise enterprise, EcoSystem business, Network network) {
      return new FinanceDepartmentOfficerWorkAreaJPanel(userProcessContainer, account, (FinanceDepartmentOfficerOrganisation)organization, enterprise, network);
    }
    
    
}