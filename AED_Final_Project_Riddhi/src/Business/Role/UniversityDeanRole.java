/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Education.Organisation.SchoolDeanOrganisation;
import Business.Education.Organisation.UniversityDeanOrganisation;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.Education.School.SchoolDeanWorkAreaJPanel;
import UserInterface.Education.University.UniversityDeanWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Riddhi
 */
public class UniversityDeanRole  extends Role{
  
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organisation, Enterprise enterprise, EcoSystem business,Network network) {
      return new UniversityDeanWorkAreaJPanel(userProcessContainer, account, (UniversityDeanOrganisation)organisation, enterprise);
    }
    
    
}