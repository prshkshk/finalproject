/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.HRD.Organisation.HRDClerkOrganisation;
import Business.HRD.Organisation.HRDOfficerOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.HRD.HRDClerkWorkAreaJPanel;
import UserInterface.HRD.HRDOfficerWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author Riddhi
 */
public class HRDOfficerRole extends Role{
  
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organization, Enterprise enterprise, EcoSystem business, Network network) {
      return new HRDOfficerWorkAreaJPanel(userProcessContainer, account, (HRDOfficerOrganisation)organization, enterprise, network);
    }
    
    
}