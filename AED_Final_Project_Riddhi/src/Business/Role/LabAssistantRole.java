/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Hospital.Organisation.LabAssistantOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import UserInterface.Hospital.LabAssistantRole.LabAssistantWorkAreaJPanel;
import javax.swing.JPanel;
/**
 *
 * @author acer
 */
public class LabAssistantRole extends Role {
   @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organisation organisation, Enterprise enterprise, EcoSystem system, Network network ) {
        return new LabAssistantWorkAreaJPanel(userProcessContainer, account, (LabAssistantOrganisation)organisation, enterprise);
    }
    
}