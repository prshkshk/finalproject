/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HealthDept.Organisation;

import Business.Organisation.Organisation;
import Business.Role.HealthDepartmentOfficerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class HealthDepartmentOfficerOrganisation extends Organisation {
    
    public HealthDepartmentOfficerOrganisation() {
        super(Organisation.Type.HealthDeptOfficer.getValue(),Organisation.Type.HealthDeptOfficer);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new HealthDepartmentOfficerRole());
        return roles;
    }
    
}
