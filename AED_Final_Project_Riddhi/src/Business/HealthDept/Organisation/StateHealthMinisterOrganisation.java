/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HealthDept.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.StateHealthMinisterRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class StateHealthMinisterOrganisation extends Organisation {

    public StateHealthMinisterOrganisation() {
        super(Organisation.Type.StateHealthMinister.getValue(),Organisation.Type.StateHealthMinister);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new StateHealthMinisterRole());
        return roles;
    }
}
