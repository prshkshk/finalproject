/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.HealthDept.Organisation;

import Business.Organisation.Organisation;
import Business.Role.HealthDepartmentClerkRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class HealthDepartmentClerkOrganisation extends Organisation {
    
    public HealthDepartmentClerkOrganisation () {
         super(Organisation.Type.HealthDeptClerk.getValue(),Organisation.Type.HealthDeptClerk);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HealthDepartmentClerkRole());
        return roles;
    }
}
