/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organisation;

import java.util.ArrayList;

/**
 *
 * @author Riddhi
 */
public class OrganisationDirectory {
     
    private ArrayList<Organisation> organisationList;

    public OrganisationDirectory() {
        organisationList = new ArrayList();
    }

    public ArrayList<Organisation> getOrganisationList() {
        return organisationList;
    }
    
    /*public Organisation createOrganisation(Type type){
        Organisation organisation = null;
        if (type.getValue().equals(Type.Doctor.getValue())){
            organisation = new DoctorOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.LabAssistant.getValue())){
            organisation = new LabAssistantOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.HRDClerk.getValue())){
            organisation = new HRDClerkOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.HRDOfficer.getValue())){
            organisation = new HRDOfficerOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.StateHRDMinister.getValue())){
            organisation = new StateHRDMinisterOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.StateHealthMinister.getValue())){
            organisation = new StateHealthMinisterOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.HealthDeptClerk.getValue())){
            organisation = new HealthDepartmentClerkOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.HealthDeptOfficer.getValue())){
            organisation = new HealthDepartmentOfficerOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.FinanceDeptOfficer.getValue())){
            organisation = new FinanceDepartmentOfficerOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.FinanceDeptClerk.getValue())){
            organisation = new FinanceDepartmentClerkOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.StateFinanceMinister.getValue())){
            organisation = new StateFinanceMinisterOragnisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.HospitalAdministrativeOffice.getValue())){
            organisation = new HospitalAdministrativeOfficeOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.Citizen.getValue())){
            organisation = new CitizenOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.SchoolAdministrativeOffice.getValue())){
            organisation = new SchoolAdministrativeOfficeOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.SchoolDean.getValue())){
            organisation = new SchoolDeanOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.UniversityAdministrativeOffice.getValue())){
            organisation = new UniversityAdministrativeOfficeOrganisation();
            organisationList.add(organisation);
        }
        else if (type.getValue().equals(Type.UniversityDean.getValue())){
            organisation = new UniversityDeanOrganisation();
            organisationList.add(organisation);
        }
        return organisation;
    }*/
      
    public Organisation createOrganisation(Organisation organisation){
       organisationList.add(organisation);
       return organisation;
    }
    
}
