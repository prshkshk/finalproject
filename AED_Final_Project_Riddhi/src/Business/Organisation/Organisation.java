/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organisation;
import Business.Employee.Employee;
import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkRequest;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Riddhi
 */
public abstract class Organisation {
      private String name;
      private WorkQueue workQueue;
      private EmployeeDirectory employeeDirectory;
      private UserAccountDirectory userAccountDirectory;
      private int organizationID;
      private static int counter;
      private Type type;
    public enum Type{
      
        
    Admin("Admin Organization"),
    HRDClerk("HRD Clerk"),
    HRDOfficer("HRD Officer"),
    StateHRDMinister("State HRD Minister"),
    HealthDeptClerk("Health Dept Clerk"),
    HealthDeptOfficer("Health Dept Officer"),
    StateHealthMinister("State Health Minister"),
    FinanceDeptClerk("Finance Dept Clerk"),
    FinanceDeptOfficer("Finance Dept Officer"),
    StateFinanceMinister("State Finance Minister"),
    UniversityDean("University Dean"),
    UniversityAdministrativeOffice("University Administrative Office"),
    SchoolDean("School Dean"),
    SchoolAdministrativeOffice("School Administrative Office"),
    Doctor("Doctor"),
    LabAssistant("Lab Assistant"),
    HospitalAdministrativeOffice("Hospital Administrative Organisation"),
    Citizen("Citizen");
    
        
        
      
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
      
        }
    
    }

  
    public Organisation(String name,Type type) {
        this.name = name;
        this.type = type;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
    
    public int calcualtetotalrequest(Organisation organisation){
       int requestcount=0;
   for(WorkRequest wr:workQueue.getWorkRequestList())
   {
   requestcount++;
   
   }
       
       return requestcount;
   
   }
   
       public float citizensatisfaction(Organisation org) {

       int count1 = 0;
       int count2 = 0;
       float avg = 0;

       if ((org.getType().getValue().equals(type.HRDClerk.getValue()))
               || (org.getType().getValue().equals(type.HealthDeptClerk.getValue()))
              ) {
           for (WorkRequest wr : org.workQueue.getWorkRequestList()) {

               GrievanceWorkRequest gwq = (GrievanceWorkRequest) wr;
               count1 = gwq.getFeedbackScore() + count1;
               count2++;

           }
           avg = count1 / count2;
           

       }
   return avg;
   
   }
   
   public int calculateCompletedrequest(Organisation organisation){
   int requestcount=0;
    for(WorkRequest wr:workQueue.getWorkRequestList())
   {
       
     if(wr.getStatus().equalsIgnoreCase("Awaiting Citizen feedback"))  
     { requestcount++;}
   
   }
       
       return requestcount;
   }

  
public float avgTimeanalysis(Organisation org,Employee emp) {
       long diff1 = 0;
       float avg = 0;
       float count = 0;
       float finalaverage = 0;

       Date requestDate;
       Date completedDate;
       for (WorkRequest wr : org.getWorkQueue().getWorkRequestList()) {
           
         
         
               if((wr.getReceiver().equals(emp.getName())) && (wr.getStatus().equalsIgnoreCase("Awaiting Citizen Response"))){
           
           requestDate = wr.getRequestDate();
           completedDate = wr.getResolveDate();

           long diff = completedDate.getTime() - requestDate.getTime();
           diff1 = diff / (1000 * 60 * 60 * 24);
           count++;
           }
           avg = avg + diff1;
       }
       finalaverage = avg / count;
       return finalaverage;
   

}
 
    @Override
    public String toString() {
        return name;
    }
    
}
