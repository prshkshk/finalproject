/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Hospital.Organisation;

import Business.Organisation.Organisation;
import Business.Role.HospitalAdministrativeOfficeRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class HospitalAdministrativeOfficeOrganisation extends Organisation {
     public HospitalAdministrativeOfficeOrganisation() {
        super(Organisation.Type.HospitalAdministrativeOffice.getValue(),Organisation.Type.HospitalAdministrativeOffice);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
       roles.add(new HospitalAdministrativeOfficeRole());
        return roles;
    }
}
