/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Hospital.Organisation;

import Business.Organisation.Organisation;
import Business.Role.LabAssistantRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class LabAssistantOrganisation extends Organisation {
    
    public LabAssistantOrganisation() {
        super(Organisation.Type.LabAssistant.getValue(),Organisation.Type.LabAssistant);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new LabAssistantRole());
        return roles;
    }
}
