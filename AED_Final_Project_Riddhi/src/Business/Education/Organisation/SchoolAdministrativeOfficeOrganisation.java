/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Education.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.SchoolAdministrativeOfficeRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class SchoolAdministrativeOfficeOrganisation extends Organisation {
    public SchoolAdministrativeOfficeOrganisation() {
        super(Organisation.Type.SchoolAdministrativeOffice.getValue(),Organisation.Type.SchoolAdministrativeOffice);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new SchoolAdministrativeOfficeRole());
        return roles;
    }
}
