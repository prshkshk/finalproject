/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Education.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.SchoolDeanRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class SchoolDeanOrganisation extends Organisation {
    
    public SchoolDeanOrganisation() {
        super(Organisation.Type.SchoolDean.getValue(),Organisation.Type.SchoolDean);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new SchoolDeanRole());
        return roles;
    }
}
