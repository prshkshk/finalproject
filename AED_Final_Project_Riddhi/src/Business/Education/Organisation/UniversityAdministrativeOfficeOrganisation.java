/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Education.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.UniversityAdministrativeOfficeRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class UniversityAdministrativeOfficeOrganisation extends Organisation {
    
    public UniversityAdministrativeOfficeOrganisation() {
        super(Organisation.Type.UniversityAdministrativeOffice.getValue(),Organisation.Type.UniversityAdministrativeOffice);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new UniversityAdministrativeOfficeRole());
        return roles;
    }
}
