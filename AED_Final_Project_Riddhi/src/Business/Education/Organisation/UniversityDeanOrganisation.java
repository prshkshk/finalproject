/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Education.Organisation;

import Business.Organisation.Organisation;
import Business.Role.Role;
import Business.Role.UniversityDeanRole;
import java.util.ArrayList;

/**
 *
 * @author prshk
 */
public class UniversityDeanOrganisation extends Organisation {
    
    public UniversityDeanOrganisation() {
        super(Organisation.Type.UniversityDean.getValue(),Organisation.Type.UniversityDean);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new UniversityDeanRole());
        return roles;
    }
    
}
