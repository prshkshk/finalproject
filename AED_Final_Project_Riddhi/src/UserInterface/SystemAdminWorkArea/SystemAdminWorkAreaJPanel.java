/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.SystemAdminWorkArea;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organisation.Organisation;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import UserInterface.SystemAdminWorkArea.ManageNetworkJPanel;
import UserInterface.SystemAdminWorkArea.ManageEnterpriseJPanel;
import UserInterface.SystemAdminWorkArea.ManageEnterpriseAdminJPanel;

/**
 *
 * @author Acer
 */
public class SystemAdminWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form SystemAdminWorkAreaJPanel
     */
    JPanel userProcessContainer;
    EcoSystem system;

    public SystemAdminWorkAreaJPanel(JPanel userProcessContainer, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.system = system;
        populateTree();
    }

    public void populateTree() {
        DefaultTreeModel model = (DefaultTreeModel) GOVERNMENT.getModel();;;

        ArrayList<Network> networkList = system.getNetworkList();
        ArrayList<Enterprise> enterpriseList;
        ArrayList<Organisation> organizationList;

        Network network;
        Enterprise enterprise;
        Organisation organization;

        DefaultMutableTreeNode networks = new DefaultMutableTreeNode("Networks");
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
        root.removeAllChildren();
        root.insert(networks, 0);

        DefaultMutableTreeNode networkNode;
        DefaultMutableTreeNode enterpriseNode;
        DefaultMutableTreeNode organizationNode;

        for (int i = 0; i < networkList.size(); i++) {
            network = networkList.get(i);
            networkNode = new DefaultMutableTreeNode(network.getName());
            networks.insert(networkNode, i);

            enterpriseList = network.getEnterpriseDirectory().getEnterpriseList();
            for (int j = 0; j < enterpriseList.size(); j++) {
                enterprise = enterpriseList.get(j);
                enterpriseNode = new DefaultMutableTreeNode(enterprise.getName());
                networkNode.insert(enterpriseNode, j);

                organizationList = enterprise.getOrganisationDirectory().getOrganisationList();

                for (int m = 0; m < organizationList.size(); m++) {
                    organization = organizationList.get(m);
                    organizationNode = new DefaultMutableTreeNode(organization.getName());
                    enterpriseNode.insert(organizationNode, m);
                }
            }
            model.reload();
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jSplitPane = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        selectedNodeLbl = new javax.swing.JLabel();
        manageNetworkJButton = new javax.swing.JButton();
        manageEnterpriseJBtn = new javax.swing.JButton();
        mngEnterpriseAdminBtn = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        GOVERNMENT = new javax.swing.JTree();

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jButton1.setText("jButton1");

        setLayout(new java.awt.BorderLayout());

        jSplitPane.setDividerLocation(150);

        jPanel2.setBackground(new java.awt.Color(0, 51, 102));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 153, 153));
        jLabel1.setText("Selected Node:");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(62, 47, -1, -1));

        selectedNodeLbl.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        selectedNodeLbl.setForeground(new java.awt.Color(153, 153, 153));
        selectedNodeLbl.setText("<view selected node>");
        jPanel2.add(selectedNodeLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(253, 47, -1, -1));

        manageNetworkJButton.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        manageNetworkJButton.setForeground(new java.awt.Color(153, 153, 153));
        manageNetworkJButton.setText("MANAGE NETWORK");
        manageNetworkJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageNetworkJButtonActionPerformed(evt);
            }
        });
        jPanel2.add(manageNetworkJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 90, 367, 100));

        manageEnterpriseJBtn.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        manageEnterpriseJBtn.setForeground(new java.awt.Color(153, 153, 153));
        manageEnterpriseJBtn.setText("MANAGE ENTERPRISE");
        manageEnterpriseJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageEnterpriseJBtnActionPerformed(evt);
            }
        });
        jPanel2.add(manageEnterpriseJBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 200, -1, 110));

        mngEnterpriseAdminBtn.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        mngEnterpriseAdminBtn.setForeground(new java.awt.Color(153, 153, 153));
        mngEnterpriseAdminBtn.setText("MANAGE ENTERPRISE ADMIN");
        mngEnterpriseAdminBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mngEnterpriseAdminBtnActionPerformed(evt);
            }
        });
        jPanel2.add(mngEnterpriseAdminBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 324, 390, 110));

        jSplitPane.setRightComponent(jPanel2);

        jPanel3.setBackground(new java.awt.Color(0, 51, 102));

        GOVERNMENT.setFont(new java.awt.Font("Adobe Devanagari", 3, 18)); // NOI18N
        GOVERNMENT.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                GOVERNMENTValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(GOVERNMENT);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(109, Short.MAX_VALUE))
        );

        jSplitPane.setLeftComponent(jPanel3);

        add(jSplitPane, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void manageNetworkJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageNetworkJButtonActionPerformed
        // TODO add your handling code here:

        ManageNetworkJPanel manageNetworkJPanel = new ManageNetworkJPanel(userProcessContainer, system);
        userProcessContainer.add("manageNetworkJPanel", manageNetworkJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_manageNetworkJButtonActionPerformed

    private void manageEnterpriseJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageEnterpriseJBtnActionPerformed
        // TODO add your handling code here:
        ManageEnterpriseJPanel manageEnterpriseJPanel = new ManageEnterpriseJPanel(userProcessContainer, system);
        userProcessContainer.add("manageEnterpriseJPanel", manageEnterpriseJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_manageEnterpriseJBtnActionPerformed

    private void mngEnterpriseAdminBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mngEnterpriseAdminBtnActionPerformed
        // TODO add your handling code here:
        ManageEnterpriseAdminJPanel manageEnterpriseAdminJPanel = new ManageEnterpriseAdminJPanel(userProcessContainer, system);
        userProcessContainer.add("manageEnterpriseAdminJPanel", manageEnterpriseAdminJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_mngEnterpriseAdminBtnActionPerformed

    private void GOVERNMENTValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_GOVERNMENTValueChanged
        // TODO add your handling code here:

        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) GOVERNMENT.getLastSelectedPathComponent();

        if (selectedNode != null) {
            selectedNodeLbl.setText(selectedNode.toString());
        }
    }//GEN-LAST:event_GOVERNMENTValueChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTree GOVERNMENT;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane;
    private javax.swing.JButton manageEnterpriseJBtn;
    private javax.swing.JButton manageNetworkJButton;
    private javax.swing.JButton mngEnterpriseAdminBtn;
    private javax.swing.JLabel selectedNodeLbl;
    // End of variables declaration//GEN-END:variables
}
