/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.HealthDept;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.HRDClerkWorkRequest;
import Business.WorkQueue.HRDOfficerWorkRequest;
import Business.WorkQueue.HealthDepartmentClerkWorkRequest;
import Business.WorkQueue.HealthMinisterWorkRequest;
import Business.WorkQueue.HealthOfficerWorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Riddhi
 */
public class ProcessHealthMinisterWorkRequestJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private HealthMinisterWorkRequest request;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private Network network;

    /**
     * Creates new form ProcessHealthMinisterWorkRequestJPanel
     */
    public ProcessHealthMinisterWorkRequestJPanel(JPanel userProcessContainer, HealthMinisterWorkRequest request, Enterprise enterprise, UserAccount userAccount, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.request = request;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.network = network;
        txtGrievanceDescription.setText(request.getHealthOfficerWorkRequest().getHealthDepartmentClerkWorkRequest().getGrievanceWorkRequest().getMessage());
        btnFeedback.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        txtGrievanceDescription = new javax.swing.JTextArea();
        btnAction = new javax.swing.JButton();
        txtAction = new javax.swing.JTextField();
        backJButton5 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnFeedback = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 51, 102));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtGrievanceDescription.setEditable(false);
        txtGrievanceDescription.setColumns(20);
        txtGrievanceDescription.setRows(5);
        jScrollPane1.setViewportView(txtGrievanceDescription);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 150, 370, 140));

        btnAction.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        btnAction.setForeground(new java.awt.Color(153, 153, 153));
        btnAction.setText("ActionTaken");
        btnAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActionActionPerformed(evt);
            }
        });
        add(btnAction, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 310, -1, -1));
        add(txtAction, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 310, 290, 29));

        backJButton5.setFont(new java.awt.Font("Oriya Sangam MN", 2, 12)); // NOI18N
        backJButton5.setForeground(new java.awt.Color(0, 51, 51));
        backJButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/back1.png"))); // NOI18N
        backJButton5.setText("<< Back");
        backJButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButton5ActionPerformed(evt);
            }
        });
        add(backJButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 30, 130, 40));

        jLabel1.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("PROCESS HEALTH MINISTER REQUEST");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 100, -1, -1));

        btnFeedback.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        btnFeedback.setForeground(new java.awt.Color(153, 153, 153));
        btnFeedback.setText("Get Citizen FeedBack");
        btnFeedback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFeedbackActionPerformed(evt);
            }
        });
        add(btnFeedback, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 360, 260, 60));

        jLabel3.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(153, 153, 153));
        jLabel3.setText("Action:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 310, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void btnActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActionActionPerformed
        // TODO add your handling code here:
        //  request.setStatus("Awaiting Citizen Feedback");
        request.setResult(txtAction.getText());
        btnFeedback.setEnabled(true);
    }//GEN-LAST:event_btnActionActionPerformed

    private void backJButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButton5ActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButton5ActionPerformed

    private void btnFeedbackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFeedbackActionPerformed
        // TODO add your handling code here:
        request.setStatus("Awaiting Citizen Feedback");
        HealthOfficerWorkRequest hrdowr = request.getHealthOfficerWorkRequest();
        HealthDepartmentClerkWorkRequest hrdcwr = hrdowr.getHealthDepartmentClerkWorkRequest();
        GrievanceWorkRequest gwr = hrdcwr.getGrievanceWorkRequest();
        gwr.setStatus("Awaiting Citizen Feedback");
        gwr.setActiontaken(request.getResult());
        JOptionPane.showMessageDialog(this, "Feedback Requested");
    }//GEN-LAST:event_btnFeedbackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton5;
    private javax.swing.JButton btnAction;
    private javax.swing.JButton btnFeedback;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtAction;
    private javax.swing.JTextArea txtGrievanceDescription;
    // End of variables declaration//GEN-END:variables
}
