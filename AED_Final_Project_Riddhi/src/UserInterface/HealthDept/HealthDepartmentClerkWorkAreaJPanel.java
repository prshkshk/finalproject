/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.HealthDept;

import Business.Enterprise.Enterprise;
import Business.HealthDept.Organisation.HealthDepartmentClerkOrganisation;
import Business.HealthDept.Organisation.HealthDepartmentOfficerOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.HealthDepartmentClerkWorkRequest;
import Business.WorkQueue.HealthOfficerWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prshk
 */
public class HealthDepartmentClerkWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private HealthDepartmentClerkOrganisation HealthDeptClerkOrganisation;
    private Network network;

    public class Thread_Escalate implements Runnable {

        private UserAccount userAccount;
        private HealthDepartmentClerkOrganisation HealthDeptClerkOrganisation;
        private Enterprise enterprise;
        boolean escalated = false;

        public Thread_Escalate(UserAccount account, HealthDepartmentClerkOrganisation organisation, Enterprise enterprise) {
            this.userAccount = account;
            this.HealthDeptClerkOrganisation = organisation;
            this.enterprise = enterprise;
        }

        @Override
        public void run() {
            while (!escalated) {
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    for (WorkRequest req : HealthDeptClerkOrganisation.getWorkQueue().getWorkRequestList()) {
                        if (req.getReceiver() == userAccount && req.getStatus().equalsIgnoreCase("Assigned to Clerk:" + userAccount)) {
                            req.setCount(req.getCount() + 1);
                            if (req.getCount() > 10) {

                                HealthDepartmentClerkWorkRequest request = (HealthDepartmentClerkWorkRequest) req;
                                request.setStatus("Sent To Officer");
                                request.setResult("Fast Forwarded");
                                HealthOfficerWorkRequest howq = new HealthOfficerWorkRequest();
                                howq.setSender(userAccount);
                                howq.setStatus("Pending");
                                howq.setMessage(request.getResult());
                                howq.setHealthDepartmentClerkWorkRequest(request);
                                GrievanceWorkRequest gwq = howq.getHealthDepartmentClerkWorkRequest().getGrievanceWorkRequest();
                                gwq.setStatus("Sent to Officer");
                                gwq.setActiontaken("Fast Forwarded");
                                gwq.setReceiver(userAccount);
                                Organisation org = null;
                                for (Organisation organisation : enterprise.getOrganisationDirectory().getOrganisationList()) {
                                    if (organisation instanceof HealthDepartmentOfficerOrganisation) {
                                        org = organisation;
                                        break;
                                    }
                                }
                                if (org != null) {
                                    org.getWorkQueue().getWorkRequestList().add(howq);

                                }

                                JOptionPane.showMessageDialog(null, "Request has been timed out and sent to the Officer");
                                escalated = true;
                                populateTable();
                                populateTableAssignToMe();
                            }
                        }
                    }

                } catch (InterruptedException ex) {

                }
            }
        }

    }

    /**
     * Creates new form HealthDeptClerkRoleJPanel
     */
    public HealthDepartmentClerkWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, HealthDepartmentClerkOrganisation organisation, Enterprise enterprise, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.HealthDeptClerkOrganisation = organisation;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.network = network;
        populateTable();
        populateTableAssignToMe();
    }

    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable1.getModel();

        model.setRowCount(0);

        for (WorkRequest request : HealthDeptClerkOrganisation.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[4];
            row[0] = request;
            row[1] = request.getSender().getEmployee().getName();
            row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[3] = request.getStatus();

            model.addRow(row);
        }
    }

    public void populateTableAssignToMe() {
        DefaultTableModel model = (DefaultTableModel) individualWorkTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : HealthDeptClerkOrganisation.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[4];
            if (request.getReceiver() == userAccount) {
                row[0] = request;
                row[1] = request.getSender().getEmployee().getName();
                row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[3] = request.getStatus();

                model.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable1 = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        individualWorkTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        btnRefresh = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 51, 102));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        workRequestJTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable1);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, 597, 162));

        assignJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        assignJButton.setForeground(new java.awt.Color(153, 153, 153));
        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });
        add(assignJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, -1, -1));

        processJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        processJButton.setForeground(new java.awt.Color(153, 153, 153));
        processJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Process.png"))); // NOI18N
        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });
        add(processJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 530, 170, 40));

        individualWorkTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(individualWorkTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, 597, 162));

        jLabel1.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("HR CLERK WORK AREA");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 30, -1, -1));

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/refresh.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        add(btnRefresh, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 20, 72, 31));
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable1.getSelectedRow();

        if (selectedRow < 0) {

            JOptionPane.showMessageDialog(this, "Please Select A Row");
            return;
        }
        WorkRequest request = (WorkRequest) workRequestJTable1.getValueAt(selectedRow, 0);
        if (request.getReceiver() != null) {
            JOptionPane.showMessageDialog(this, "Already assigned to :" + request.getReceiver());
            return;
        }
        request.setReceiver(userAccount);
        request.setStatus("Assigned to Clerk:" + userAccount);
        HealthDepartmentClerkWorkRequest hcwq = (HealthDepartmentClerkWorkRequest) request;
        GrievanceWorkRequest gwq = hcwq.getGrievanceWorkRequest();
        gwq.setStatus(request.getStatus());
        gwq.setReceiver(userAccount);
        Thread_Escalate escalation = new Thread_Escalate(userAccount, HealthDeptClerkOrganisation, enterprise);
        Thread escalationThread = new Thread(escalation);
        escalationThread.start();

        populateTable();
        populateTableAssignToMe();

    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed
        int selectedRow = individualWorkTable.getSelectedRow();
        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row From your table");
            return;
        }

        HealthDepartmentClerkWorkRequest request = (HealthDepartmentClerkWorkRequest) individualWorkTable.getValueAt(selectedRow, 0);
        if (!request.getStatus().equalsIgnoreCase("Assigned to Clerk:" + userAccount)) {
            JOptionPane.showMessageDialog(this, "Cannot Process This Request");
            return;
        }
        request.setStatus("Processing");
        HealthDepartmentClerkWorkRequest hcwq = (HealthDepartmentClerkWorkRequest) request;
        GrievanceWorkRequest gwq = hcwq.getGrievanceWorkRequest();
        gwq.setStatus("Processing by Clerk");
        ProcessHealthClerkWorkRequestJPanel processWorkRequestJPanel = new ProcessHealthClerkWorkRequestJPanel(userProcessContainer, request, enterprise, userAccount, network);
        userProcessContainer.add("processHealthClerkWorkRequestJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();

        layout.next(userProcessContainer);

    }//GEN-LAST:event_processJButtonActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        populateTable();
        populateTableAssignToMe();
    }//GEN-LAST:event_btnRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JTable individualWorkTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton processJButton;
    private javax.swing.JTable workRequestJTable1;
    // End of variables declaration//GEN-END:variables
}
