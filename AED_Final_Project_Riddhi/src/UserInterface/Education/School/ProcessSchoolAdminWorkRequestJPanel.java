/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Education.School;

//import Business.WorkQueue.LabTestWorkRequest;
import Business.Education.Organisation.SchoolDeanOrganisation;
import Business.Enterprise.Enterprise;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.SchoolAdminWorkRequest;
import Business.WorkQueue.SchoolDeanWorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Riddhi
 */
public class ProcessSchoolAdminWorkRequestJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ProcessSchoolAdminWorkRequestJPanel
     */
    private JPanel userProcessContainer;
    private SchoolAdminWorkRequest request;
    private Enterprise enterprise;
    private UserAccount userAccount;
    public ProcessSchoolAdminWorkRequestJPanel(JPanel userProcessContainer, SchoolAdminWorkRequest request, Enterprise enterprise, UserAccount userAccount) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.request = request;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        if(request.gethRDOfficerWorkRequest() != null){
            txtGrievanceDescription.setText(request.gethRDOfficerWorkRequest().getResult());
        }
        else{
            txtGrievanceDescription.setText(request.gethRDClerkWorkRequest().getResult());
        }
        btnSendToDean.setEnabled(false);
        submitJButton.setEnabled(false);
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        resultJTextField = new javax.swing.JTextField();
        submitJButton = new javax.swing.JButton();
        backJButton2 = new javax.swing.JButton();
        btnSendToDean = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtGrievanceDescription = new javax.swing.JTextArea();
        btnAction = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 51, 102));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 153, 153));
        jLabel1.setText("Action Taken");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 300, -1, 40));
        add(resultJTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 300, 188, 34));

        submitJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        submitJButton.setForeground(new java.awt.Color(153, 153, 153));
        submitJButton.setText("Submit To Gov");
        submitJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitJButtonActionPerformed(evt);
            }
        });
        add(submitJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 380, -1, 43));

        backJButton2.setFont(new java.awt.Font("Oriya Sangam MN", 2, 12)); // NOI18N
        backJButton2.setForeground(new java.awt.Color(0, 51, 51));
        backJButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/back1.png"))); // NOI18N
        backJButton2.setText("<< Back");
        backJButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButton2ActionPerformed(evt);
            }
        });
        add(backJButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 26, 130, 40));

        btnSendToDean.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        btnSendToDean.setForeground(new java.awt.Color(153, 153, 153));
        btnSendToDean.setText("Send To Dean");
        btnSendToDean.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendToDeanActionPerformed(evt);
            }
        });
        add(btnSendToDean, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 380, 170, 40));

        txtGrievanceDescription.setEditable(false);
        txtGrievanceDescription.setColumns(20);
        txtGrievanceDescription.setRows(5);
        jScrollPane1.setViewportView(txtGrievanceDescription);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 100, 360, 140));

        btnAction.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        btnAction.setForeground(new java.awt.Color(153, 153, 153));
        btnAction.setText("Action Taken");
        btnAction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActionActionPerformed(evt);
            }
        });
        add(btnAction, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 300, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void submitJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitJButtonActionPerformed
        if(request.gethRDOfficerWorkRequest() != null){
        request.setStatus("Assigned To Officer:"+request.getSender());
        request.gethRDOfficerWorkRequest().setStatus("Assigned To Officer:"+request.getSender());
        }
        else{
            request.setStatus("Assigned To Clerk:"+request.getSender());
        }
        request.gethRDClerkWorkRequest().setStatus("Assigned To Clerk:"+request.getSender());
        request.gethRDClerkWorkRequest().getGrievanceWorkRequest().setStatus("Assigned To Clerk:"+request.getSender());
    }//GEN-LAST:event_submitJButtonActionPerformed

    private void backJButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButton2ActionPerformed

       userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        SchoolAdministrationWorkAreaJPanel dwjp = (SchoolAdministrationWorkAreaJPanel) component;
        dwjp.populateSchoolAdminRequestTable();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButton2ActionPerformed

    private void btnSendToDeanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendToDeanActionPerformed
        // TODO add your handling code here:
         request.setStatus("Sent To School Dean");

            SchoolDeanWorkRequest howq = new SchoolDeanWorkRequest();
            howq.setSender(userAccount);
            howq.setStatus("Pending");
            howq.setMessage(request.getTestResult());
            howq.setSchoolAdminWorkRequest(request);
             if(request.gethRDOfficerWorkRequest() != null){
                 request.gethRDOfficerWorkRequest().setStatus("Sent to School Dean");
             }
             request.gethRDClerkWorkRequest().setStatus("Sent to School Dean");
            GrievanceWorkRequest gwq=  howq.getSchoolAdminWorkRequest().gethRDClerkWorkRequest().getGrievanceWorkRequest();
            gwq.setStatus("Sent to School Dean");
            gwq.setReceiver(userAccount);
            
            Organisation o = null;
            for (Organisation organisation : enterprise.getOrganisationDirectory().getOrganisationList()){
                if (organisation instanceof SchoolDeanOrganisation){
                    o = organisation;
                    break;
                }
            }
            if (o!=null){
                o.getWorkQueue().getWorkRequestList().add(howq);
                JOptionPane.showMessageDialog(this,"Sent to Dean");
            }

            

           
        else
        {

            JOptionPane.showMessageDialog(this,"Cannot Contact School");
            return;

        }

    }//GEN-LAST:event_btnSendToDeanActionPerformed

    private void btnActionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActionActionPerformed
        // TODO add your handling code here:
        request.setTestResult(resultJTextField.getText());
        btnSendToDean.setEnabled(true);
        submitJButton.setEnabled(true);
    }//GEN-LAST:event_btnActionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton2;
    private javax.swing.JButton btnAction;
    private javax.swing.JButton btnSendToDean;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField resultJTextField;
    private javax.swing.JButton submitJButton;
    private javax.swing.JTextArea txtGrievanceDescription;
    // End of variables declaration//GEN-END:variables
}
