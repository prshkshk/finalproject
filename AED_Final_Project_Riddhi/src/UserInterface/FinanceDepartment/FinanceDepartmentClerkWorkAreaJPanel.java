/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.FinanceDepartment;

import Business.Enterprise.Enterprise;
import Business.FinanceDepartment.Organisation.FinanceDepartmentClerkOrganisation;
import Business.FinanceDepartment.Organisation.FinanceDepartmentOfficerOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.FinanceDepartmentClerkWorkRequest;
import Business.WorkQueue.FinanceDepartmentOfficerWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prshk
 */
public class FinanceDepartmentClerkWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private FinanceDepartmentClerkOrganisation FinanceDepartmentClerkOrganisation;
    private Enterprise enterprise;
    private Network network;

    public class Thread_Escalate implements Runnable {

        private UserAccount userAccount;
        private FinanceDepartmentClerkOrganisation FinanceDepartmentClerkOrganisation;
        private Enterprise enterprise;
        boolean escalated = false;

        public Thread_Escalate(UserAccount account, FinanceDepartmentClerkOrganisation organisation, Enterprise enterprise) {
            this.userAccount = account;
            this.FinanceDepartmentClerkOrganisation = organisation;
            this.enterprise = enterprise;
        }

        @Override
        public void run() {
            while (!escalated) {
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    for (WorkRequest req : FinanceDepartmentClerkOrganisation.getWorkQueue().getWorkRequestList()) {
                        if (req.getReceiver() == userAccount && req.getStatus().equalsIgnoreCase("Assigned to Clerk:" + userAccount)) {
                            req.setCount(req.getCount() + 1);
                            if (req.getCount() > 10) {

                                FinanceDepartmentClerkWorkRequest request = (FinanceDepartmentClerkWorkRequest) req;
                                request.setStatus("Sent To Officer");
                                FinanceDepartmentOfficerWorkRequest howq = new FinanceDepartmentOfficerWorkRequest();
                                howq.setSender(userAccount);
                                howq.setStatus("Pending");
                                howq.setMessage(request.getMessage());
                                howq.sethRDClerkWorkRequest(request);
                                GrievanceWorkRequest gwq = howq.getfinanceClerkWorkRequest().getGrievanceWorkRequest();
                                gwq.setStatus("Sent to Officer");
                                gwq.setReceiver(userAccount);
                                Organisation org = null;
                                for (Organisation organisation : enterprise.getOrganisationDirectory().getOrganisationList()) {
                                    if (organisation instanceof FinanceDepartmentOfficerOrganisation) {
                                        org = organisation;
                                        break;
                                    }
                                }
                                if (org != null) {
                                    org.getWorkQueue().getWorkRequestList().add(howq);
                                }

                                JOptionPane.showMessageDialog(null, "Request has been timed out and sent to the Officer");
                                escalated = true;
                                populateTable();
                                populateTableAssignToMe();
                            }
                        }
                    }

                } catch (InterruptedException ex) {

                }
            }
        }

    }

    /**
     * Creates new form FinanceDepartmentClerkRoleJPanel
     */
    public FinanceDepartmentClerkWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, FinanceDepartmentClerkOrganisation organisation, Enterprise enterprise, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.FinanceDepartmentClerkOrganisation = organisation;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.network = network;
        populateTable();
        populateTableAssignToMe();
    }

    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable1.getModel();

        model.setRowCount(0);

        for (WorkRequest request : FinanceDepartmentClerkOrganisation.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[4];
            row[0] = request;
            row[1] = request.getSender().getEmployee().getName();
            row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[3] = request.getStatus();

            model.addRow(row);
        }
    }

    public void populateTableAssignToMe() {
        DefaultTableModel model = (DefaultTableModel) induvidualworkRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : FinanceDepartmentClerkOrganisation.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[4];
            if (request.getReceiver() == userAccount) {
                row[0] = request;
                row[1] = request.getSender().getEmployee().getName();
                row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[3] = request.getStatus();

                model.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        induvidualworkRequestJTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        workRequestJTable1 = new javax.swing.JTable();
        btnRefresh = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 51, 102));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        induvidualworkRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(induvidualworkRequestJTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 329, 597, 162));

        assignJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        assignJButton.setForeground(new java.awt.Color(153, 153, 153));
        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });
        add(assignJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 238, 194, 41));

        processJButton.setFont(new java.awt.Font("Kefa", 0, 13)); // NOI18N
        processJButton.setForeground(new java.awt.Color(153, 153, 153));
        processJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Process.png"))); // NOI18N
        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });
        add(processJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 509, 140, 48));

        workRequestJTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(workRequestJTable1);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 57, 597, 163));

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/refresh.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        add(btnRefresh, new org.netbeans.lib.awtextra.AbsoluteConstraints(781, 17, 72, 31));

        jLabel1.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("FINANCE CLERK WORK AREA");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(177, 17, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable1.getSelectedRow();

        if (selectedRow < 0) {

            JOptionPane.showMessageDialog(this, "Please Select A Row");
            return;
        }

        WorkRequest request = (WorkRequest) workRequestJTable1.getValueAt(selectedRow, 0);
        if (request.getReceiver() == null) {
            request.setReceiver(userAccount);
            request.setStatus("Assigned to Clerk:" + userAccount);
            FinanceDepartmentClerkWorkRequest fcwq = (FinanceDepartmentClerkWorkRequest) request;
            GrievanceWorkRequest gwq = fcwq.getGrievanceWorkRequest();
            Thread_Escalate escalation = new Thread_Escalate(userAccount, FinanceDepartmentClerkOrganisation, enterprise);
            Thread escalationThread = new Thread(escalation);
            escalationThread.start();
            gwq.setStatus(request.getStatus());
            gwq.setReceiver(userAccount);
        } else {
            JOptionPane.showMessageDialog(this, "Already Assigned to a Clerk");
            return;
        }

        populateTable();
        populateTableAssignToMe();

    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = induvidualworkRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row From your table");
            return;
        }

        FinanceDepartmentClerkWorkRequest request = (FinanceDepartmentClerkWorkRequest) induvidualworkRequestJTable.getValueAt(selectedRow, 0);

        request.setStatus("Processing");

        ProcessClerkWorkRequestJPanel processWorkRequestJPanel = new ProcessClerkWorkRequestJPanel(userProcessContainer, request, enterprise, userAccount);
        userProcessContainer.add("processfinanceclerkWorkRequestJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();

        layout.next(userProcessContainer);

    }//GEN-LAST:event_processJButtonActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        populateTable();
        populateTableAssignToMe();
    }//GEN-LAST:event_btnRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JTable induvidualworkRequestJTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton processJButton;
    private javax.swing.JTable workRequestJTable1;
    // End of variables declaration//GEN-END:variables
}
