/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.FinanceDepartment;

import UserInterface.FinanceDepartment.*;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.FinanceDepartment.Organisation.FinanceDepartmentClerkOrganisation;
import Business.FinanceDepartment.Organisation.FinanceDepartmentOfficerOrganisation;
import Business.FinanceDepartment.Organisation.StateFinanceDepartmentMinisterOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FinanceDepartmentClerkWorkRequest;
import Business.WorkQueue.FinanceDepartmentMinisterWorkRequest;
import Business.WorkQueue.FinanceDepartmentOfficerWorkRequest;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prshk
 */
public class FinanceDepartmentOfficerWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private FinanceDepartmentOfficerOrganisation FinanceDepartmentOfficerOrganisation;
    private Network network;

    public class Thread_Escalate implements Runnable {

        private UserAccount userAccount;
        private FinanceDepartmentOfficerOrganisation FinanceDepartmentOfficerOrganisation;
        private Enterprise enterprise;
        boolean escalated = false;

        public Thread_Escalate(UserAccount account, FinanceDepartmentOfficerOrganisation organisation, Enterprise enterprise) {
            this.userAccount = account;
            this.FinanceDepartmentOfficerOrganisation = organisation;
            this.enterprise = enterprise;
        }

        @Override
        public void run() {
            while (!escalated) {
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    for (WorkRequest req : FinanceDepartmentOfficerOrganisation.getWorkQueue().getWorkRequestList()) {
                        if (req.getReceiver() == userAccount && req.getStatus().equalsIgnoreCase("Assigned to Clerk:" + userAccount)) {
                            req.setCount(req.getCount() + 1);
                            if (req.getCount() > 10) {
                                FinanceDepartmentOfficerWorkRequest request = (FinanceDepartmentOfficerWorkRequest) req;
                                request.setStatus("Sent To Minister");
                                FinanceDepartmentMinisterWorkRequest hmwq = new FinanceDepartmentMinisterWorkRequest();

                                hmwq.setSender(userAccount);
                                hmwq.setStatus("Pending");
                                hmwq.setMessage(request.getMessage());
                                hmwq.sethRDOfficerWorkRequest(request);

                                FinanceDepartmentClerkWorkRequest hcwq = hmwq.gethRDOfficerWorkRequest().getfinanceClerkWorkRequest();
                                hcwq.setStatus("Sent To Minister");

                                GrievanceWorkRequest gwq = hcwq.getGrievanceWorkRequest();
                                gwq.setStatus("Pending with FinanceDepartment Minister");
                                gwq.setReceiver(userAccount);
                                Organisation org = null;
                                for (Organisation organisation : enterprise.getOrganisationDirectory().getOrganisationList()) {
                                    if (organisation instanceof StateFinanceDepartmentMinisterOrganisation) {
                                        org = organisation;
                                        break;
                                    }
                                }
                                if (org != null) {
                                    org.getWorkQueue().getWorkRequestList().add(hmwq);
                                    userAccount.getWorkQueue().getWorkRequestList().add(hmwq);
                                }

                                JOptionPane.showMessageDialog(null, "Request has been timed out and sent to the Officer");
                                escalated = true;
                                populateTable();
                                populateTableAssignToMe();
                            }
                        }
                    }

                } catch (InterruptedException ex) {

                }
            }
        }

    }

    /**
     * Creates new form FinanceDepartmentOfficerWorkAreaJPanel
     */
    public FinanceDepartmentOfficerWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, FinanceDepartmentOfficerOrganisation organisation, Enterprise enterprise, Network network) {
        initComponents();
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.FinanceDepartmentOfficerOrganisation = organisation;
        this.enterprise = enterprise;
        this.userAccount = account;
        populateTable();
        populateTableAssignToMe();
    }

    public void populateTable() {

        DefaultTableModel modelT2 = (DefaultTableModel) timedOutWorkRequestJTable.getModel();
        modelT2.setRowCount(0);

        for (WorkRequest request : FinanceDepartmentOfficerOrganisation.getWorkQueue().getWorkRequestList()) {
            FinanceDepartmentOfficerWorkRequest hcwq = (FinanceDepartmentOfficerWorkRequest) request;
            Object[] row = new Object[5];
            row[0] = request;
            row[1] = hcwq.getfinanceClerkWorkRequest().getSender().getEmployee().getName();
            row[2] = request.getSender().getEmployee().getName();
            row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[4] = request.getStatus();

            modelT2.addRow(row);
        }
    }

    public void populateTableAssignToMe() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : FinanceDepartmentOfficerOrganisation.getWorkQueue().getWorkRequestList()) {
            if (request.getReceiver() == userAccount) {
                Object[] row = new Object[4];
                row[0] = request;
                row[1] = request.getSender().getEmployee().getName();
                row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[3] = request.getStatus();

                model.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        timedOutWorkRequestJTable = new javax.swing.JTable();
        btnRefresh = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 51, 102));

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);

        assignJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        assignJButton.setForeground(new java.awt.Color(153, 153, 153));
        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });

        processJButton.setForeground(new java.awt.Color(153, 153, 153));
        processJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Process.png"))); // NOI18N
        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });

        timedOutWorkRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Assigned Clerk", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(timedOutWorkRequestJTable);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/refresh.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("FINANCE OFFICER WORK AREA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(186, 186, 186)
                .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
            .addGroup(layout.createSequentialGroup()
                .addGap(124, 124, 124)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(processJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(assignJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 597, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(assignJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(processJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = timedOutWorkRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please slecet a row");
            return;
        }

        WorkRequest request = (WorkRequest) timedOutWorkRequestJTable.getValueAt(selectedRow, 0);
        if (request.getReceiver() == null) {
            request.setReceiver(userAccount);
            request.setStatus("Assigned To Officer:" + userAccount);
        } else {
            JOptionPane.showMessageDialog(this, "Already Assigned to a Officer");
            return;
        }
        populateTable();
        populateTableAssignToMe();
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row From your table");
            return;
        }

        FinanceDepartmentOfficerWorkRequest request = (FinanceDepartmentOfficerWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);

        request.setStatus("Processing by Officer:" + userAccount);

        ProcessOfficerWorkRequestJPanel processWorkRequestJPanel = new ProcessOfficerWorkRequestJPanel(userProcessContainer, request, enterprise, userAccount);
        userProcessContainer.add("ProcessOfficeWorkRequestJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();

        layout.next(userProcessContainer);
    }//GEN-LAST:event_processJButtonActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        populateTable();
        populateTableAssignToMe();
    }//GEN-LAST:event_btnRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton processJButton;
    private javax.swing.JTable timedOutWorkRequestJTable;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
