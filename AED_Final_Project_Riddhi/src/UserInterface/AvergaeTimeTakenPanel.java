/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.EcoSystem;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organisation.Organisation;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Riddhi
 */
public class AvergaeTimeTakenPanel extends javax.swing.JPanel {

    /**
     * Creates new form AvergaeTimeTakenPanel
     */
    JPanel userProcessContainer;
    EcoSystem system;
    public AvergaeTimeTakenPanel(JPanel userProcessContainer, EcoSystem system) {
        initComponents();
          this.userProcessContainer=userProcessContainer;
        this.system=system;
    }



    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        chartPanel = new javax.swing.JPanel();
        getChart = new javax.swing.JButton();
        ChartPanel1 = new javax.swing.JPanel();
        backJButton5 = new javax.swing.JButton();

        chartPanel.setBackground(new java.awt.Color(153, 153, 153));
        chartPanel.setLayout(new java.awt.BorderLayout());

        getChart.setText("BarChart");
        getChart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getChartActionPerformed(evt);
            }
        });

        ChartPanel1.setLayout(new java.awt.BorderLayout());

        backJButton5.setFont(new java.awt.Font("Oriya Sangam MN", 2, 12)); // NOI18N
        backJButton5.setForeground(new java.awt.Color(0, 51, 51));
        backJButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/back1.png"))); // NOI18N
        backJButton5.setText("<< Back");
        backJButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(ChartPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(getChart)
                        .addGap(427, 427, 427))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chartPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)))
                .addContainerGap(126, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(backJButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(getChart))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(backJButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1)
                .addComponent(chartPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addComponent(ChartPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void getChartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getChartActionPerformed
        // TODO add your handling code here:
        float a=0;
float a1 = 0;
float a2 = 0;
float a3 = 0;
float a4 = 0;
float a5;float a6;
       
        float b=0;

        for(Network n:system.getNetworkList())
            
        {
            if(n.getName().equalsIgnoreCase("Mass"))
            {
            for(Enterprise e:n.getEnterpriseDirectory().getEnterpriseList())
            {

                if(e.getEnterpriseType().getValue().equalsIgnoreCase(Enterprise.EnterpriseType.Government.getValue()))
                {

                    for(Organisation o:e.getOrganisationDirectory().getOrganisationList())
                    {
                       if(o.getType().getValue().equalsIgnoreCase(Organisation.Type.HRDClerk.getValue())){
                           
                       for(Employee emp:o.getEmployeeDirectory().getEmployeeList())
                       {
                       if(emp.getName().equalsIgnoreCase("HRClerk1"))
                       {
                           a1=o.avgTimeanalysis(o,emp);
                       }
                       else if(emp.getName().equalsIgnoreCase("HRClerk2"))
                       {
                           a2=o.avgTimeanalysis(o,emp);
                       }
                      
                       }
                        
                        
                        }

                    }

                }
            }

        }
            else if(n.getName().equalsIgnoreCase("NY"))
        
       
        {
            for(Enterprise e:n.getEnterpriseDirectory().getEnterpriseList())
            {

                if(e.getEnterpriseType().getValue().equalsIgnoreCase(Enterprise.EnterpriseType.Government.getValue()))
                {

                    for(Organisation o:e.getOrganisationDirectory().getOrganisationList())
                    {
                        if(o.getType().getValue().equalsIgnoreCase(Organisation.Type.HRDClerk.getValue())){
                           
                       for(Employee emp:o.getEmployeeDirectory().getEmployeeList())
                       {
                       if(emp.getName().equalsIgnoreCase("HRClerk1"))
                       {
                           a3=o.avgTimeanalysis(o,emp);
                       }
                       else if(emp.getName().equalsIgnoreCase("HRClerk2"))
                       {
                           a4=o.avgTimeanalysis(o,emp);
                       }
                      
                       }
                        
                        
                        }
                        

                    }

                }
            }

        }
        }

        DefaultCategoryDataset barChartData = new DefaultCategoryDataset();
        barChartData.setValue(a1, "Total Request", "HR Department");
        barChartData.setValue(a2, "Total Request", "Health Department");
        JFreeChart barChart = ChartFactory.createBarChart("Mass", "ClerkOrganisation", "Predicted Time", barChartData, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot barchrt = barChart.getCategoryPlot();
        barchrt.setRangeGridlinePaint(Color.BLUE);
        ChartPanel barP = new ChartPanel(barChart);
        barP.setVisible(true);
        chartPanel.removeAll();
        chartPanel.add(barP, BorderLayout.CENTER);
        chartPanel.validate();

        DefaultCategoryDataset barChartData1 = new DefaultCategoryDataset();
        barChartData1.setValue(a3, "Total  Request", "HRD");
        barChartData1.setValue(a4, "Total Request", "Health");
        JFreeChart barChart1 = ChartFactory.createBarChart("NY", "ClerkOrganisation", "Predicted Time", barChartData, PlotOrientation.VERTICAL, false, true, false);
        CategoryPlot barchrt1 = barChart1.getCategoryPlot();
        barchrt1.setRangeGridlinePaint(Color.ORANGE);
        ChartPanel barP1 = new ChartPanel(barChart1);
        barP1.setVisible(true);
        ChartPanel1.removeAll();
        ChartPanel1.add(barP1, BorderLayout.CENTER);
        ChartPanel1.validate();

    }//GEN-LAST:event_getChartActionPerformed

    private void backJButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButton5ActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButton5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ChartPanel1;
    private javax.swing.JButton backJButton5;
    private javax.swing.JPanel chartPanel;
    private javax.swing.JButton getChart;
    // End of variables declaration//GEN-END:variables
}
