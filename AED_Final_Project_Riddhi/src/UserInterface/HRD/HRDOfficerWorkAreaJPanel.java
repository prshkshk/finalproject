/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.HRD;

import Business.Enterprise.Enterprise;
import Business.HRD.Organisation.HRDOfficerOrganisation;
import Business.HRD.Organisation.StateHRDMinisterOrganisation;
import Business.Network.Network;
import Business.Organisation.Organisation;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.GrievanceWorkRequest;
import Business.WorkQueue.HRDClerkWorkRequest;
import Business.WorkQueue.HRDMinisterWorkRequest;
import Business.WorkQueue.HRDOfficerWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author prshk
 */
public class HRDOfficerWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private HRDOfficerOrganisation HRDOfficerOrganisation;
    private Network network;

    public class Thread_Escalate implements Runnable {

        private UserAccount userAccount;
        private HRDOfficerOrganisation HRDOfficerOrganisation;
        private Enterprise enterprise;
        boolean escalated = false;

        public Thread_Escalate(UserAccount account, HRDOfficerOrganisation organisation, Enterprise enterprise) {
            this.userAccount = account;
            this.HRDOfficerOrganisation = organisation;
            this.enterprise = enterprise;
        }

        @Override
        public void run() {
            while (!escalated) {
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    for (WorkRequest req : HRDOfficerOrganisation.getWorkQueue().getWorkRequestList()) {
                        if (req.getReceiver() == userAccount && req.getStatus().equalsIgnoreCase("Assigned to Officer:" + userAccount)) {
                            req.setCount(req.getCount() + 1);
                            if (req.getCount() > 10) {
                                HRDOfficerWorkRequest request = (HRDOfficerWorkRequest) req;
                                request.setStatus("Sent To Minister");
                                request.setResult("Fast Forwarded");
                                HRDMinisterWorkRequest hmwq = new HRDMinisterWorkRequest();

                                hmwq.setSender(userAccount);
                                hmwq.setStatus("Pending");
                                hmwq.setMessage(request.getResult());
                                hmwq.sethRDOfficerWorkRequest(request);

                                HRDClerkWorkRequest hcwq = hmwq.gethRDOfficerWorkRequest().gethRDClerkWorkRequest();
                                hcwq.setStatus("Sent To Minister");
                                hcwq.setResult("Fast Forwarded");
                                GrievanceWorkRequest gwq = hcwq.getGrievanceWorkRequest();
                                gwq.setStatus("Pending with HRD Minister");
                                gwq.setActiontaken("Fast Forwarded");
                                gwq.setReceiver(userAccount);
                                Organisation org = null;
                                for (Organisation organisation : enterprise.getOrganisationDirectory().getOrganisationList()) {
                                    if (organisation instanceof StateHRDMinisterOrganisation) {
                                        org = organisation;
                                        break;
                                    }
                                }
                                if (org != null) {
                                    org.getWorkQueue().getWorkRequestList().add(hmwq);
                                    userAccount.getWorkQueue().getWorkRequestList().add(hmwq);
                                }
                                JOptionPane.showMessageDialog(null, "Request has been timed out and sent to the Minister");
                                escalated = true;
                                populateTable();
                                populateTableAssignToMe();
                            }
                        }
                    }

                } catch (InterruptedException ex) {

                }
            }
        }

    }

    /**
     * Creates new form HRDOfficerWorkAreaJPanel
     */
    public HRDOfficerWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, HRDOfficerOrganisation organisation, Enterprise enterprise, Network network) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.HRDOfficerOrganisation = organisation;
        this.enterprise = enterprise;
        this.userAccount = account;
        populateTable();
        populateTableAssignToMe();
    }

    public void populateTable() {

        DefaultTableModel modelT2 = (DefaultTableModel) timedOutWorkRequestJTable.getModel();
        modelT2.setRowCount(0);

        for (WorkRequest request : HRDOfficerOrganisation.getWorkQueue().getWorkRequestList()) {
            HRDOfficerWorkRequest hcwq = (HRDOfficerWorkRequest) request;
            Object[] row = new Object[5];
            row[0] = request;
            row[1] = hcwq.gethRDClerkWorkRequest().getSender().getEmployee().getName();
            row[2] = request.getSender().getEmployee().getName();
            row[3] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
            row[4] = request.getStatus();

            modelT2.addRow(row);
        }
    }

    public void populateTableAssignToMe() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : HRDOfficerOrganisation.getWorkQueue().getWorkRequestList()) {
            if (request.getReceiver() == userAccount) {
                Object[] row = new Object[4];
                row[0] = request;
                row[1] = request.getSender().getEmployee().getName();
                row[2] = request.getReceiver() == null ? null : request.getReceiver().getEmployee().getName();
                row[3] = request.getStatus();

                model.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        timedOutWorkRequestJTable = new javax.swing.JTable();
        btnRefresh = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(0, 51, 102));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 350, 597, 162));

        assignJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        assignJButton.setForeground(new java.awt.Color(153, 153, 153));
        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });
        add(assignJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 300, 160, 40));

        processJButton.setFont(new java.awt.Font("Kefa", 1, 18)); // NOI18N
        processJButton.setForeground(new java.awt.Color(153, 153, 153));
        processJButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Process.png"))); // NOI18N
        processJButton.setText("Process");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });
        add(processJButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 530, 170, 40));

        timedOutWorkRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Message", "Sender", "Assigned Clerk", "Receiver", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(timedOutWorkRequestJTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, 597, 162));

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/refresh.png"))); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });
        add(btnRefresh, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 30, 72, 31));

        jLabel1.setFont(new java.awt.Font("Malayalam MN", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("HR OFFICER WORK AREA");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 30, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = timedOutWorkRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Please slecet a row");
            return;
        }

        WorkRequest request = (WorkRequest) timedOutWorkRequestJTable.getValueAt(selectedRow, 0);
        if (request.getReceiver() != null) {
            JOptionPane.showMessageDialog(this, "Already assigned to :" + request.getReceiver());
            return;
        }
        request.setReceiver(userAccount);
        request.setStatus("Assigned To Officer:" + userAccount);
        HRDOfficerWorkRequest hrdowr = (HRDOfficerWorkRequest) request;
        HRDClerkWorkRequest hcwq = hrdowr.gethRDClerkWorkRequest();
        hcwq.setReceiver(userAccount);
        hcwq.setStatus(request.getStatus());
        GrievanceWorkRequest gwq = hcwq.getGrievanceWorkRequest();
        gwq.setStatus(request.getStatus());
        gwq.setReceiver(userAccount);

        Thread_Escalate escalation = new Thread_Escalate(userAccount, HRDOfficerOrganisation, enterprise);
        Thread escalationThread = new Thread(escalation);
        escalationThread.start();
        populateTable();
        populateTableAssignToMe();
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(this, "Select a row From your table");
            return;
        }

        HRDOfficerWorkRequest request = (HRDOfficerWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);

        request.setStatus("Processing by Officer:" + userAccount);
        HRDOfficerWorkRequest hrdowr = (HRDOfficerWorkRequest) request;
        HRDClerkWorkRequest hcwq = hrdowr.gethRDClerkWorkRequest();
        GrievanceWorkRequest gwq = hcwq.getGrievanceWorkRequest();
        gwq.setStatus("Processing by Officer");
        ProcessOfficerWorkRequestJPanel processWorkRequestJPanel = new ProcessOfficerWorkRequestJPanel(userProcessContainer, request, enterprise, userAccount, network);
        userProcessContainer.add("ProcessOfficeWorkRequestJPanel", processWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();

        layout.next(userProcessContainer);
    }//GEN-LAST:event_processJButtonActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        // TODO add your handling code here:
        populateTable();
        populateTableAssignToMe();
    }//GEN-LAST:event_btnRefreshActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton processJButton;
    private javax.swing.JTable timedOutWorkRequestJTable;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
